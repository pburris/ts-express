import { Request, Response, NextFunction } from 'express';
import JWTService from '../services/JWTService';
import AppException from '../AppException';

export default function handleError(req: Request, res: Response, next: NextFunction) {
    const token = req.body.token || req.headers['x-auth-token'] || req.query.token;

    try {
        if (!token)
            throw new AppException(403, '400003', 'no token present on request', 'no token present on request');
        if (!JWTService.verify(token))
            throw new AppException(403, '400003', 'token invalid', 'token invalid');
        next();
    } catch (e) {
        res.status(e.httpStatusCode).send({ exception: e });
    }
}