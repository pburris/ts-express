import { Request, Response, NextFunction } from 'express';
import AppException from '../AppException';
import { ExpressJoiError } from 'express-joi-validation';

function instanceOfJoiError(object: any): object is ExpressJoiError {
    return 'type' in object && 'error' in object;
}

export default function handleError(err: AppException | ExpressJoiError, req: Request, res: Response, next: NextFunction) {
    if (err && instanceOfJoiError(err)) {
        const msg = `${err.error.toString()} on type ${err.type}`;
        const appErr = new AppException(400, '400001', msg, msg);
        return res.status(appErr.httpStatusCode).json({
            exception: appErr,
        });
    }
    res.status(err.httpStatusCode).send({ exception: err });
}