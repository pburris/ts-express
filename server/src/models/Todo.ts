import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Todo {
    @PrimaryGeneratedColumn()
    public id!: number;

    @Column()
    public task: string = '';

    @Column()
    public completed: boolean = false;
    
}

export default Todo;