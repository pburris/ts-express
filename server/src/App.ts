import express from 'express';
import { createConnection, Connection } from 'typeorm';
import morgan from 'morgan';
import AppRouter from './AppRouter';
import AppConfig from './AppConfig';
import handleError from './middleware/handleError';

export default class App {
    public app: express.Application;
    public connection: Connection | null = null;

    constructor() {
        this.app = express();

        // Connect to DB
        this.initDatabase()
            .then(() => this.initMiddlware())
            .catch(e => console.log(e));
    }

    private async initDatabase() {
        if (!this.connection) {
            this.connection = await createConnection();
        }
    }

    private initMiddlware() {
        // Body Parsing
        this.app.use(express.urlencoded({ extended: false }));
        this.app.use(express.json());

        // Logging
        this.app.use(morgan('dev'));

        // Routes
        this.app.use(AppRouter.getInstance());

        // Error Handling
        this.app.use(handleError);
    }
    
    public listen(cb?: Function) {
        this.app.listen(AppConfig.port, () => {
            console.log(`Express application running on port ${AppConfig.port}`);
            if (cb) cb();
        });
    }
}
