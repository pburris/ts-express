
export default class AppException extends Error {

    public httpStatusCode: number;
    public code: string;
    public message: string;
    public detail: string;
  
    constructor(httpStatusCode: number, code: string, message: string, detail: string) {
      super(message);
      this.httpStatusCode = httpStatusCode;
      this.code = code;
      this.message = message;
      this.detail = detail;
    }
};

export interface IAppException {
  httpStatusCode: number;
  code: string;
  message: string;
  detail: string;
}