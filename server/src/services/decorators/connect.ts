import 'reflect-metadata';
import { getConnection, createConnection } from 'typeorm';

interface ConnectionObject {
    [key: string]: Function;
}

export function connect(obj: ConnectionObject) {
    return function(target: Function) {
        createConnection()
            .then(() => {
                const conn = getConnection();
                target.prototype.repositories = Object
                    .keys(obj)
                    .reduce((acc, i) => ({
                        ...acc,
                        [i]: conn.getRepository(obj[i]),
                    }), {});
            });
    }
}
