import 'reflect-metadata';
import BaseService from './BaseService';
import Todo from '../models/Todo';
import { connect } from './decorators/index';
import { Repository } from 'typeorm';


@connect({ todo: Todo })
export default class TodoService extends BaseService {

    public static async getTodos(): Promise<Todo[]> {
        const repository: Repository<Todo> = this.prototype.repositories.todo;
        const todos = await repository.find();
        return todos || [];
    }

    public static async getTodo(id: number): Promise<Todo | undefined> {
        const repository: Repository<Todo> = this.prototype.repositories.todo;
        const todo = await repository.findOne(id);
        return todo;
    }

    public static async createTodo(todo: Todo): Promise<Todo> {
        const repository: Repository<Todo> = this.prototype.repositories.todo;
        const t = new Todo();
        t.task = todo.task;
        t.completed = todo.completed;
        await repository.save(t);
        return t;
    }

    public static async updateTodo(id: number, update: Todo): Promise<Todo | undefined> {
        const repository: Repository<Todo> = this.prototype.repositories.todo;
        await repository.update(id, update)
        const updated = await repository.findOne(id);
        return updated;
    }

    public static async deleteTodo(id: number): Promise<Todo | undefined>  {
        const repository: Repository<Todo> = this.prototype.repositories.todo;
        const old = await repository.findOne(id);
        await repository.delete(id);
        return old;
    }
}