import jwt from 'jsonwebtoken';
import AppConfig from '../AppConfig';
import BaseService from './BaseService';

const { issuer, audience, expiresIn, algorithm, privateKey, publicKey } = AppConfig.jwt;

export default class JWTService extends BaseService {
    private static signOptions = { issuer, audience, expiresIn, algorithm };
    private static verifyOptions = { issuer, audience, expiresIn, algorithm: [algorithm] };

    public static sign(payload: any): string {
        return jwt.sign(payload, privateKey, this.signOptions);
    }

    public static verify(token: string): boolean {
        try {
            return !!jwt.verify(token, publicKey, this.verifyOptions);
        } catch (e) {
            return false;
        }
    }

    public static decode(token: string): any {
        return jwt.decode(token, { complete: true }) || {};
    }
}