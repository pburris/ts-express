import 'reflect-metadata';
import BaseService from './BaseService';
import User from '../models/User';
import { connect } from './decorators/index';
import { Repository, UpdateResult } from 'typeorm';


@connect({ user: User })
export default class UserService extends BaseService {

    public static async getUsers(): Promise<User[]> {
        const repository: Repository<User> = this.prototype.repositories.user;
        const users = await repository.find();
        return users || [];
    }

    public static async getUser(id: string): Promise<User | undefined> {
        const repository: Repository<User> = this.prototype.repositories.user;
        const user = await repository.findOne(id);
        return user;
    }

    public static async createUser(user: any): Promise<User> {
        const repository: Repository<User> = this.prototype.repositories.user;
        const t = new User();
        t.username = user.username;
        t.password = user.password;
        await repository.save(t);
        return t;
    }

    public static async updateUser(id: string, update: User): Promise<User | undefined> {
        const repository: Repository<User> = this.prototype.repositories.user;
        await repository.update(id, update)
        const updated = await repository.findOne(id);
        return updated;
    }

    public static async deleteUser(id: string): Promise<User | undefined>  {
        const repository: Repository<User> = this.prototype.repositories.user;
        const old = await repository.findOne(id);
        await repository.delete(id);
        return old;
    }

    public static async getUserByName(username: string): Promise<User | undefined> {
        const repository: Repository<User> = this.prototype.repositories.user;
        const user = await repository.findOne({ where: { username } });
        return user;
    }

    public static async updateUserByName(username: string, update: User): Promise<User | undefined> {
        const repository: Repository<User> = this.prototype.repositories.user;
        await repository.update({ username }, update);
        return await repository.findOne({ where: { username } });;
    }

    public static async deleteUserByName(username: string): Promise<User | undefined> {
        const repository: Repository<User> = this.prototype.repositories.user;
        const user = await repository.findOne({ where: { username } });
        if (user) await repository.delete(user.id);
        return user;
    }
}