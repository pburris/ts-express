import { Repository } from "typeorm";

interface RepositoryObject {
    [key: string]: Repository<any>;
}

export default class BaseService {
    repositories: RepositoryObject = {};
}