import superagent from 'superagent';
import User from '../models/User';
import UserService from '../services/UserService';
import AppConfig from '../AppConfig';

describe("Authentication Controller", () => {
    let user: User;
    const userData = { username: 'user1', password: 'password' };
    beforeEach(async (done) => {
        user = await UserService.createUser(userData as User);
        done();
    });

    afterEach(async (done) => {
        const users: User[] = await UserService.getUsers();
        for (let i = 0; i < users.length; i++) {
            await UserService.deleteUser(users[i].id);
        }
        done();
    })

    it("can sign up", async (done) => {
        superagent
            .post(`http://${AppConfig.host}:${AppConfig.port}/authentication/signup`)
            .send({ username: 'user_example', password: 'password', passwordCheck: 'password' })
            .then(res => {
                expect(res.status).toEqual(200);
                expect(res.body.token).toBeDefined();
                done();
            });
    });

    it("can sign in", async (done) => {
        superagent
            .post(`http://${AppConfig.host}:${AppConfig.port}/authentication/signin`)
            .send(userData)
            .then(res => {
                expect(res.status).toEqual(200);
                expect(res.body.token).toBeDefined();
                done();
            });
    });

    it("can refresh user token", async (done) => {
        superagent
            .post(`http://${AppConfig.host}:${AppConfig.port}/authentication/signin`)
            .send(userData)
            .then(res => {
                expect(res.status).toEqual(200);
                expect(res.body.token).toBeDefined();
                const token = res.body.token;

                superagent
                    .post(`http://${AppConfig.host}:${AppConfig.port}/authentication/token`)
                    .send({ token })
                    .then(res => {
                        expect(res.status).toEqual(200);
                        done();
                    });
            });
    });
});