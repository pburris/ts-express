import superagent from 'superagent';
import AppConfig from '../AppConfig';

describe("Public Controller", () => {

    it("can GET /ping", done => {
        superagent
            .get(`http://${AppConfig.host}:${AppConfig.port}/ping`)
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.status).toEqual(200);
                expect(res.body.up).toBe(true);
                done();
            })
            .catch(e => {
                fail(e);
                done();
            });
    });
});