import TodoService from "../services/TodoService";
import Todo from "../models/Todo";

describe("Todo Service", () => {

    afterEach(async (done) => {
        const todos: Todo[] = await TodoService.getTodos();
        for (let i = 0; i < todos.length; i++) {
            await TodoService.deleteTodo(todos[i].id);
        }
        done();
    });

    it('can create and fetch todos', async (done) => {
        const todo = await TodoService.createTodo({ task: 'task name', completed: true } as Todo);
        const todo2 = await TodoService.createTodo({ task: 'task 2', completed: false } as Todo);
        const getTodo = await TodoService.getTodo(todo.id);
        const getTodo2 = await TodoService.getTodo(todo2.id);
        if (getTodo && getTodo2) {
            expect(todo.id).toEqual(getTodo.id);
            expect(todo.completed).toEqual(getTodo.completed);
            expect(todo.task).toEqual(getTodo.task);

            expect(todo2.id).toEqual(getTodo2.id);
            expect(todo2.completed).toEqual(getTodo2.completed);
            expect(todo2.task).toEqual(getTodo2.task);
        } else {
            fail('Could not create new todo');
        }

        done();
    });

    it('can create and delete todos', async (done) => {
        const todo = await TodoService.createTodo({ task: 'task name', completed: true } as Todo);
        const todo2 = await TodoService.createTodo({ task: 'task 2', completed: false } as Todo);

        const getTodos = await TodoService.getTodos();
        expect(getTodos.length).toBe(2);

        const getTodo = await TodoService.deleteTodo(todo.id);
        const getTodo2 = await TodoService.deleteTodo(todo2.id);
        if (getTodo && getTodo2) {
            expect(todo.id).toEqual(getTodo.id);
            expect(todo.completed).toEqual(getTodo.completed);
            expect(todo.task).toEqual(getTodo.task);

            expect(todo2.id).toEqual(getTodo2.id);
            expect(todo2.completed).toEqual(getTodo2.completed);
            expect(todo2.task).toEqual(getTodo2.task);

            const getTodos2 = await TodoService.getTodos();
            expect(getTodos2.length).toBe(0);
        } else {
            fail('Could not delete todo');
        }
        done();
    });

    it('can create and edit todos', async (done) => {
        const todo = await TodoService.createTodo({ task: 'task name', completed: true } as Todo);
        const todo2 = await TodoService.createTodo({ task: 'task 2', completed: false } as Todo);
        const editTodo = await TodoService.updateTodo(todo.id, { task: 'update' } as Todo);
        const editTodo2 = await TodoService.updateTodo(todo2.id, { completed: true } as Todo);

        if (editTodo && editTodo2) {
            expect(todo.id).toEqual(editTodo.id);
            expect(todo.completed).toEqual(editTodo.completed);
            expect(editTodo.task).toEqual('update');

            expect(todo2.id).toEqual(editTodo2.id);
            expect(editTodo2.completed).toEqual(true);
            expect(todo2.task).toEqual(editTodo2.task);
        } else {
            fail('Could not update todos');
        }
        done();
    });
});