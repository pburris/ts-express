import UserService from "../services/UserService";
import User from "../models/User";

describe("User Service", () => {

    let user: User;
    let user2: User;
    beforeEach(async (done) => {
        user = await UserService.createUser({ username: 'user1', password: 'password' });
        user2 = await UserService.createUser({ username: 'user2', password: 'password' });
        done();
    });

    afterEach(async (done) => {
        const users: User[] = await UserService.getUsers();
        for (let i = 0; i < users.length; i++) {
            await UserService.deleteUser(users[i].id);
        }
        done();
    });

    it('can create users', async (done) => {
        let users = await UserService.getUsers();
        expect(users.length).toEqual(2);
        await UserService.createUser({ username: 'user3', password: 'password' });
        await UserService.createUser({ username: 'user4', password: 'password' });
        users = await UserService.getUsers();
        expect(users.length).toEqual(4);
        done();
    });

    it('can fetch users', async (done) => {
        const getUser = await UserService.getUser(user.id);
        const getUser2 = await UserService.getUser(user2.id);

        if (getUser && getUser2) {
            expect(user.id).toEqual(getUser.id);
            expect(user.username).toEqual(getUser.username);
            expect(user.password).toEqual(getUser.password);

            expect(user2.id).toEqual(getUser2.id);
            expect(user2.username).toEqual(getUser2.username);
            expect(user2.password).toEqual(getUser2.password);
            done();
        } else {
            fail('Could not create new user');
            done();
        }
    });

    it('can update users', async (done) => {
        const updateUser = await UserService.updateUser(user.id, { password: 'pass' } as User);
        const updateUser2 = await UserService.updateUser(user2.id, { password: 'pass' } as User);

        if (updateUser && updateUser2) {
            expect(user.id).toEqual(updateUser.id);
            expect(user.username).toEqual(updateUser.username);
            expect(updateUser.password).toEqual('pass');

            expect(user2.id).toEqual(updateUser2.id);
            expect(user2.username).toEqual(updateUser2.username);
            expect(updateUser2.password).toEqual('pass');
            done();
        } else {
            fail('Could not update users');
            done();
        }
    });

    it('can delete users', async (done) => {
        const getUsers = await UserService.getUsers();
        expect(getUsers.length).toBe(2);

        const getUser = await UserService.deleteUser(user.id);
        const getUser2 = await UserService.deleteUser(user2.id);
        if (getUser && getUser2) {
            expect(user.id).toEqual(getUser.id);
            expect(user.password).toEqual(getUser.password);
            expect(user.username).toEqual(getUser.username);

            expect(user2.id).toEqual(getUser2.id);
            expect(user2.password).toEqual(getUser2.password);
            expect(user2.username).toEqual(getUser2.username);

            const getUsers2 = await UserService.getUsers();
            expect(getUsers2.length).toBe(0);
        } else {
            fail('Could not delete user');
        }
        done();
    });

    it('can fetch users by username', async (done) => {    
        const getUser = await UserService.getUserByName(user.username);
        const getUser2 = await UserService.getUserByName(user2.username);

        if (getUser && getUser2) {
            expect(user.id).toEqual(getUser.id);
            expect(user.username).toEqual(getUser.username);
            expect(user.password).toEqual(getUser.password);

            expect(user2.id).toEqual(getUser2.id);
            expect(user2.username).toEqual(getUser2.username);
            expect(user2.password).toEqual(getUser2.password);
            done();
        } else {
            fail('Could not create new user');
            done();
        } 
    });

    it('can update users by username', async (done) => {  
        const updateUser = await UserService.updateUserByName(user.username, { password: 'pass' } as User);
        const updateUser2 = await UserService.updateUserByName(user2.username, { password: 'pass' } as User);

        if (updateUser && updateUser2) {
            expect(user.id).toEqual(updateUser.id);
            expect(user.username).toEqual(updateUser.username);
            expect(updateUser.password).toEqual('pass');

            expect(user2.id).toEqual(updateUser2.id);
            expect(user2.username).toEqual(updateUser2.username);
            expect(updateUser2.password).toEqual('pass');
            done();
        } else {
            fail('Could not update users');
            done();
        }
    });

    it('can delete users by username', async (done) => {
        const getUsers = await UserService.getUsers();
        expect(getUsers.length).toBe(2);

        const getUser = await UserService.deleteUserByName(user.username);
        const getUser2 = await UserService.deleteUserByName(user2.username);
        if (getUser && getUser2) {
            expect(user.id).toEqual(getUser.id);
            expect(user.password).toEqual(getUser.password);
            expect(user.username).toEqual(getUser.username);

            expect(user2.id).toEqual(getUser2.id);
            expect(user2.password).toEqual(getUser2.password);
            expect(user2.username).toEqual(getUser2.username);

            const getUsers2 = await UserService.getUsers();
            expect(getUsers2.length).toBe(0);
        } else {
            fail('Could not delete user');
        }
        done();
    });

});