import superagent from 'superagent';
import AppConfig from '../AppConfig';
import TodoService from '../services/TodoService';
import Todo from '../models/Todo';

describe("Todos Controller", () => {
    let todo: Todo;
    beforeEach(async (done) => {
        todo = await TodoService.createTodo({ task: 'This is a task', completed: false } as Todo);
        done();
    });

    afterEach(async (done) => {
        const todos: Todo[] = await TodoService.getTodos();
        for (let i = 0; i < todos.length; i++) {
            await TodoService.deleteTodo(todos[i].id);
        }
        done();
    })

    it("can GET /todo", done => {
        superagent
            .get(`http://${AppConfig.host}:${AppConfig.port}/todo`)
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.status).toEqual(200);
                expect(res.body.todos.length).toBe(1);
                expect(res.body.todos[0].task).toBe(todo.task);
                expect(res.body.todos[0].completed).toBe(todo.completed);
                done();
            });
    });

    it("can GET /todo/:id", done => {
        superagent
        .get(`http://${AppConfig.host}:${AppConfig.port}/todo/${todo.id}`)
        .set('Accept', 'application/json')
        .then(res => {
            expect(res.status).toEqual(200);
            expect(res.body.todo.task).toBe(todo.task);
            expect(res.body.todo.completed).toBe(todo.completed);
            done();
        });
    });

    it("can POST /todo", done => {
        superagent
        .post(`http://${AppConfig.host}:${AppConfig.port}/todo`)
        .send({ todo: { task: 'new task', completed: true } })
        .set('Accept', 'application/json')
        .then(res => {
            expect(res.status).toEqual(200);
            expect(res.body.todo.task).toBe('new task');
            expect(res.body.todo.completed).toBe(true);
            done();
        });
    });

    it("can PUT /todo/:id", done => {
        superagent
        .put(`http://${AppConfig.host}:${AppConfig.port}/todo/${todo.id}`)
        .send({ todo: { task: 'updated', completed: true } })
        .set('Accept', 'application/json')
        .then(() => {
            superagent
            .get(`http://${AppConfig.host}:${AppConfig.port}/todo/${todo.id}`)
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.status).toEqual(200);
                expect(res.body.todo.task).toBe('updated');
                expect(res.body.todo.completed).toBe(true);
                done();
            })
            .catch(e => {
                fail(e);
                done();
            })
        });
    });

 
    it("can DELETE /todo/:id", done => {
        superagent
        .del(`http://${AppConfig.host}:${AppConfig.port}/todo/${todo.id}`)
        .set('Accept', 'application/json')
        .then(() => {
            superagent
            .get(`http://${AppConfig.host}:${AppConfig.port}/todo`)
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.body.todos.length).toEqual(0);
                done();
            });
        });
    });
});