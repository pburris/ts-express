import JWTService from "../services/JWTService";

describe("JWT Service", () => {
    it("can create a signed JWT with a payload", () => {
        const payload = { username: "username", id: 13 };

        const jwt = JWTService.sign(payload);
        expect(jwt).toBeTruthy();
        
        const verified = JWTService.verify(jwt);
        expect(verified).toBe(true);

        const decoded = JWTService.decode(jwt);
        expect(decoded.payload.username).toEqual('username');
        expect(decoded.payload.id).toEqual(13);
    });
});