import User from '../models/User';
import UserService from '../services/UserService';
import AppConfig from '../AppConfig';
import superagent from 'superagent';

describe("User Controller", () => {
    let user: User;
    beforeEach(async (done) => {
        user = await UserService.createUser({ username: 'user1', password: 'password' } as User);
        done();
    });

    afterEach(async (done) => {
        const users: User[] = await UserService.getUsers();
        for (let i = 0; i < users.length; i++) {
            await UserService.deleteUser(users[i].id);
        }
        done();
    })

    it("Can GET /users", async (done) => {
        superagent
            .get(`http://${AppConfig.host}:${AppConfig.port}/users`)
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.status).toEqual(200);
                expect(res.body.users).toBeDefined();
                expect(res.body.users.length).toBe(1);
                done();
            });
    });

    it("can GET /user/:username", async (done) => {
        superagent
            .get(`http://${AppConfig.host}:${AppConfig.port}/users/${user.username}`)
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.status).toEqual(200);
                done();
            });  
    });

    it("can PUT /user/:username", async (done) => {
        superagent
            .put(`http://${AppConfig.host}:${AppConfig.port}/users/${user.username}`)
            .send({ username: 'newuser' })
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.status).toEqual(200);
                done();
            });  
    });

    it("can DELETE /user/:username", async (done) => {
        superagent
            .delete(`http://${AppConfig.host}:${AppConfig.port}/users/${user.username}`)
            .set('Accept', 'application/json')
            .then(res => {
                expect(res.status).toEqual(200);
                done();
            });  
    });
});