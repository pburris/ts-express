import App from './App';

// Controllers
import './controllers/PublicController';
import './controllers/TodoController';
import './controllers/AuthenticationController';
import './controllers/UserController';

new App().listen();