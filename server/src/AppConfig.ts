import fs from 'fs';

interface Env {
    port: string;
    host: string;
    nodeEnv: string;
    jwt: JWTConfig;
}

interface JWTConfig {
    issuer: string;
    audience: string;
    expiresIn: string;
    algorithm: string;
    publicKey: string;
    privateKey: string;
}

const extractEnv = (env: any): Env => {
    return {
        port: env.PORT || '3000',
        host: env.HOST || '0.0.0.0',
        nodeEnv: env.NODE_ENV || 'development',
        jwt: {
            issuer: env.JWT_ISSUER || 'Todo/Authentication',
            audience: env.JWT_AUDIENCE || 'Main_App',
            expiresIn: env.JWT_EXPIRESIN || '4h',
            algorithm: env.JWT_ALGORITHM || 'RS256',
            publicKey: fs.readFileSync(env.JWT_PUBLIC_KEY || 'src/keys/jwtRS256.key.pub', 'utf8'),
            privateKey: fs.readFileSync(env.JWT_PRIVATE_KEY || 'src/keys/jwtRS256.key', 'utf8')
        }
    } as Env;
}

const env = extractEnv(process.env);

export default class AppConfig {
    public static port: number = parseInt(env.port);
    public static host: string = env.host;
    public static nodeEnv: string = env.nodeEnv;
    public static jwt: JWTConfig = env.jwt;
}