import { Request, Response } from 'express';
import * as Joi from '@hapi/joi';
import { controller, get, post, validateBody, put, del, use } from './decorators/index';
import JWTService from '../services/JWTService';
import UserService from '../services/UserService';
import AppException from '../AppException';
import checkToken from '../middleware/checkToken';

const signupBody = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
    passwordCheck: Joi.string().required()
});

const signinBody = Joi.object({
    username: Joi.string().required(),
    password: Joi.string().required(),
});

const newTokenBody = Joi.object({
    token: Joi.string().required(),
});

@controller('/authentication')
export default class TodoController {

    @post('/signup')
    @validateBody(signupBody)
    async signup(req: Request, res: Response) {
        try {
            const { username, password, passwordCheck } = req.body;
            const user = await UserService.getUserByName(username);
            if (user)
                throw new AppException(401, '400100', 'username already in use', 'username already in use');
            if (password !== passwordCheck)
                throw new AppException(401, '400100', 'Passwords do not match', 'passwords do not match');
            
            const newUser = await UserService.createUser({ username, password });
            const token = JWTService.sign({ username: newUser.username, id: newUser.id });
            res.send({ token });

        } catch(e) {
            res.status(e.httpStatusCode)
                .send({ exception: e })
        }
    }

    @post('/signin')
    @validateBody(signinBody)
    async signin(req: Request, res: Response) {
        const { username, password } = req.body;
        const user = await UserService.getUserByName(username);
        if (!user || password !== user.password)
            throw new AppException(401, '400100', 'username/password combo incorrect', 'username/password combo incorrect');
        res.send({ token: JWTService.sign({ username: user.username, id: user.id }) });
    }

    @post('/token')
    @use(checkToken)
    @validateBody(newTokenBody)
    async newToken(req: Request, res: Response) {
        try {
            const token = req.body.token || req.headers['x-auth-token'] || req.query.token;
            if (!JWTService.verify(token))
                throw new AppException(401, '400100', 'invalid token', 'invalid token');

            const decoded = JWTService.decode(token);
            const user = await UserService.getUser(decoded.payload.id);

            if (!user)
                throw new AppException(401, '400100', 'invalid token', 'invalid token');

            res.send({ token: JWTService.sign({ id: user.id, username: user.username }) });
        } catch (e) {
            res.status(e.httpStatusCode)
                .send({ exception: e});
        }
    }
}