import { Request, Response } from 'express';
import * as Joi from '@hapi/joi';
import { controller, get, post, validateBody, put, del } from './decorators/index';
import TodoService from '../services/TodoService';

const todoSchema = Joi.object({
    id: Joi.string(),
    task: Joi.string().required(),
    completed: Joi.bool().required(),
});

const todoBody = Joi.object({
    todo: todoSchema.required()
});

@controller('/todo')
export default class TodoController {

    @get('/')
    async getTodos(_: Request, res: Response) {
        const todos = await TodoService.getTodos();
        res.send({ todos });
    }

    @get('/:id')
    async getTodo(req: Request, res: Response) {
        const todo = await TodoService.getTodo(req.params.id);
        res.send({ todo });
    }

    @post('/')
    @validateBody(todoBody)
    async createTodo(req: Request, res: Response) {
        const todo = await TodoService.createTodo(req.body.todo);
        res.send({ todo });
    }

    @put('/:id')
    @validateBody(todoBody)
    async updateTodo(req: Request, res: Response) {
        const id = req.params.id;
        const todo = await TodoService.updateTodo(id, req.body.todo);
        res.send({ todo });
    }

    @del('/:id')
    async deleteTodo(req: Request, res: Response) {
        const deleted = await TodoService.deleteTodo(req.params.id); 
        res.send({ deleted });
    }
}