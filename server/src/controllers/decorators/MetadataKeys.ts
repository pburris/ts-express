enum MetadataKeys {
    method = 'method',
    path = 'path',
    middleware = 'middleware',
    validators = 'validators',
};

export default MetadataKeys;