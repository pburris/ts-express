import 'reflect-metadata';
import express from 'express';
import AppRouter from '../../AppRouter';
import MetadataKeys from './MetadataKeys';
import Methods from './Methods';

function isValidMethod(method: string): method is keyof typeof express.Router {
    return method.toLowerCase() in express.Router;
}

export function controller(pathPrefix: string) {
    return function(target: Function) {
        const router = AppRouter.getInstance();
        for (let key in target.prototype) {
            const routeHandler = target.prototype[key];
            const method = Reflect.getMetadata(MetadataKeys.method, target.prototype, key);
            const path = Reflect.getMetadata(MetadataKeys.path, target.prototype, key);
            const middlewares = Reflect.getMetadata(MetadataKeys.middleware, target.prototype, key) || [];
            const validators = Reflect.getMetadata(MetadataKeys.validators, target.prototype, key) || [];

            if (method && path) {
                const fullPath = `${pathPrefix !== '/' ? pathPrefix : ''}${path}`;
                const m = isValidMethod(method) ? method : Methods.get;
                const fn: Function = router[m];

                fn.call(router, fullPath, ...middlewares, ...validators, routeHandler);
            }
        }
    }
}
