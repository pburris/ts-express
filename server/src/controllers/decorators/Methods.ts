enum Methods {
    get = 'get',
    post = 'post',
    put = 'put',
    patch = 'patch',
    option = 'option',
    del = 'delete',
    head = 'head'
}

export default Methods;