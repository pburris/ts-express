import 'reflect-metadata';
import MetadataKeys from './MetadataKeys';
import * as Joi from '@hapi/joi';
import { createValidator, ExpressJoiInstance } from 'express-joi-validation'

enum validatorTypes {
    body = 'body',
    headers = 'headers',
    fields = 'feilds',
    params = 'params',
    query = 'query',
}

const validator: ExpressJoiInstance = createValidator({ passError: true });

function isValidValidatorType(type: string): type is keyof ExpressJoiInstance {
    return type in validator;
}

function validate(type: string) {
    return function validateType(schema: Joi.ObjectSchema) {
        return function(target: any, key: string, desc: PropertyDescriptor) {
            const validators = Reflect.getMetadata(
                MetadataKeys.validators,
                target,
                key
            ) || [];

            if (isValidValidatorType(type)) {
                const fn: Function = validator[type];
                Reflect.defineMetadata(
                    MetadataKeys.validators,
                    [...validators, fn.call(validator, schema)],
                    target,
                    key
                );
            }
        }
    }
}


export const validateBody = validate(validatorTypes.body);
export const validateHeaders = validate(validatorTypes.headers);
export const validateFields = validate(validatorTypes.fields);
export const validateParams = validate(validatorTypes.params);
export const validateQuery = validate(validatorTypes.query);