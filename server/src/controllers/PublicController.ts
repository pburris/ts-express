import { Request, Response } from 'express';
import { controller, get, use } from './decorators/index';
import checkToken from '../middleware/checkToken';


@controller('/')
export default class PublicController {

    @get('/ping')
    async getIndex(_: Request, res: Response) {
        res.send({ up: true });
    }
    
}