import { Request, Response } from 'express';
import * as Joi from '@hapi/joi';
import { controller, get, post, validateBody, put, del, use } from './decorators/index';
import UserService from '../services/UserService';
import AppException from '../AppException';

const userSchema = Joi.object({
    id: Joi.string(),
    username: Joi.string().required(),
    password: Joi.string().required(),
});

const userBody = Joi.object({
    user: userSchema.required()
});

@controller('/users')
export default class UserController {

    @get('/')
    async getUsers(_: Request, res: Response) {
        const users = await UserService.getUsers();
        res.send({ users });
    }

    @post('/')
    @validateBody(userBody)
    async createUser(req: Request, res: Response) {
        const user = await UserService.createUser(req.body.user);
        res.json({ user });
    }

    @get('/:username')
    async getUser(req: Request, res: Response) {
        const username = req.params.username;
        const user = await UserService.getUser(username);
        res.json({ user });
    }

    @put('/:username')
    @validateBody(userBody)
    async updateUser(req: Request, res: Response) {
        const username = req.params.username;
        const user = await UserService.getUser(username);
        if (!user)
            throw new AppException(401, '400100', 'user not found', 'user not found');
        
        const newUser = await UserService.updateUserByName(username, req.body.user);
        res.json({ user: newUser });
    }

    @del('/:username')
    async deleteUser(req: Request, res: Response) {
        const username = req.params.username;
        const user = await UserService.getUser(username);
        if (!user)
            throw new AppException(401, '400100', 'user not found', 'user not found'); 
        
        const resp = await UserService.deleteUserByName(username);
        res.json({ user: resp });
    }
}