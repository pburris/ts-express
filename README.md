# Full stack Typescript

Express backend with PostgreSQL and TypeORM. React and Redux on the frontend.

## Warning
Do not use the keys in this project, generate your own:
```sh
ssh-keygen -t rsa -b 4096 -m PEM -f jwtRS256.key
# Don't add passphrase
openssl rsa -in jwtRS256.key -pubout -outform PEM -out jwtRS256.key.pub
cat jwtRS256.key
cat jwtRS256.key.pub
```