FROM node:10-stretch

RUN useradd -ms /bin/bash express_app
WORKDIR /home/express_app

ENV PATH /home/express_app/node_modules/.bin:$PATH

COPY server/package.json package.json
RUN npm install --silent

COPY server .

CMD ["npm", "start"]